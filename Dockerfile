# Use the official Ansible image
FROM ansible/ansible:latest

# Copy your playbook and inventory file into the container
COPY playbooks/* /ansible/*
COPY inventory.ini /ansible/inventory.ini

# Set the working directory
WORKDIR /ansible