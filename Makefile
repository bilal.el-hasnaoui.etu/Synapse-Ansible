venv:
	if ! [ -d venv ]; then python3 -m venv venv; fi;
	./venv/bin/pip install -r requirements.txt

install:
	ansible-galaxy install -r requirements.yml

action:
	act -W .gitea/workflows/on_pr.yml
