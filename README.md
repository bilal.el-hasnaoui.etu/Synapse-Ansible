# TP ASR - Ansible - Nginx - Synapse
- Ce TP a pour but d'automatiser l'ensemble des configurations d'une machine en utilisant **Ansible** , en minimisant au maximum les interactions "human to computer".

- Ce fichier README va servir comme un résumé ou un "summary" qui regroupe les grandes étapes qu'on a pu achevé pendant ce TP.

- L'ensemble des commandes et outils utilisés sont présents dans le repository ci-dessus.

## I. Pré-configuration
### 1. Virtualenv
Il s'agit d'un environnement virtuel de python, séparé du système de fichiers de la 'zabeth' pour faire les choses proprement.

### 2. Ansible
Ansible est un outil de gestion de configuration et d'automatisation qui permet de déployer, configurer et orchestrer des infrastructures informatiques de manière efficace et reproductible.

### 3. Vérification de la configuration d'un playbook
- `ansible-playbook` utilisée pour exécuter des scripts YAML, appelés playbooks, afin de définir et d'automatiser des tâches de gestion de configuration sur des machines distantes.
A chaque configuration d'un playbook, on a toujours intérêt à la vérifier avec `ansible-playbook`.

## II. Configuration du serveur DATABASE
- Il s'agit d'une configuration d'un serveur de base de données avec Ansible incluant l'automatisation de l'installation et de la configuration de `PostgreSQL`.

- Cela nous a permit de garantir une configuration cohérente et reproductible, ainsi que la gestion des utilisateurs, des autorisations, des sauvegardes et de la sécurité, le tout dans un environnement scalable et facilement maintenable.

## III. Configuration du serveur web/Synapse
- Automatiser l'installation et la configuration de Nginx pour servir notre page web statique.

- Assurer la sécurisation du serveur en configurant des certificats avec l'utilitaire `certbot` pour sécuriser les communications HTTPS.
### 1. Synapse
Le logiciel synapse avait pour but dans ce TP de :
- Fournir une plateforme de messagerie instantanée décentralisée et sécurisée (riot).

- Permettre la communication en temps réel entre les utilisateurs via des salons de discussion, des messages directs... (Dernière étape du TP)

- Assurer la synchronisation des données entre les différents serveurs Matrix pour une expérience utilisateur cohérente.

### 2. Nginx reverse proxy
- Le reverse proxy avait pour but d'utiliser la certificat TLS (générée avec certbot) qui écoute sur le port 443 et sur notre servername `synapse.nyala.website` (voir dans le fichier `playbooks/matrix.conf.j2`).

- Pour le rôle du reverse proxy en sécurité, on cite **La gestion de la sécurité** : Il agit comme une barrière entre les clients et les serveurs back-end, offrant une couche de sécurité supplémentaire en masquant les détails de l'infrastructure interne et en fournissant des fonctionnalités de protection contre les attaques DDoS, les injections SQL...



----
Ce projet a été développé par **EL HASNAOUI Bilal** & **CHAOUNI Ayoub**, sous l'encadrement de **M. MAURICE Thomas** et **M. REDON Xavier**.




